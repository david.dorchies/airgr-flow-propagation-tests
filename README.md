
<!-- README.md is generated from README.Rmd. Please edit that file -->

``` r
library(ggplot2)
library(airGR.FlowPropagationTests)
#> Loading required package: rsic2
```

# airGR.FlowPropagationTests

<!-- badges: start -->
<!-- badges: end -->

The goal of airGR.FlowPropagationTests is to simulate flood time series
propagation through several configuration of river confluences with a
full Saint-Venant 1D model.

## Installation

You can install the development version of airGR.FlowPropagationTests
like so:

``` r
# install.packages("devtools")
devtools::install_deps()
devtools::load_all()
```

## River configurations

We tests the following configurations representative of contrasted
situations:

### Study case \#1

This configuration is equivalent to one upstream node connected to one
downstream node with a single uniform reach between the two nodes. This
study case tests the hability to reproduce correctly the deformation of
a peak.

### Study case \#2

The catchment is composed of two upstream nodes: an upstream node
flowing in an homogeneous reach and a second node connected on the last
third of the reach.

This study cases tests the hability to reproduce two peaks due to
different distances of upstream nodes and the intensification of the
deformation due to a greater distance.

### Study case \#3

The catchment is composed of two upstream reaches of same lengths and
characteristics which flow in a single reach of same lengths and
characteristics than the other two.

this configuration is derived from study case \#1 but with upstream
nodes located far away from the confluency.

### Study case \#4

Same as \#3, except that one upstream reach has different
characteristics (slope, width and roughness) which results in a two-peak
flood.

### Study case \#5

Same as \#4, except that the confluency is immediatly upstream the
downstream node. The two upstream contributions flow in two reaches of
same length and different characteristics.

These two last cases are the worst since the semi-distributive model can
only count on the distance between the downstream node and the upstream
ones to differenciate the flow routing.

### General configurations for the hydraulic model

All river configurations have the following characteristics:

-   2 upstream gauging stations
-   1 confluence
-   1 downstream gauging station

It results in 3 reaches interconnecting 4 nodes. Each of reach
![i](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;i "i")
is characterized with the following parameters:

-   The slope
    ![S_i](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;S_i "S_i")
    (m/km)
-   The lenght
    ![L_i](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;L_i "L_i")
    (km)
-   The bed width
    ![B_i](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;B_i "B_i")
    (m)
-   The roughness
    ![K_i](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;K_i "K_i")
    (Strickler coefficient)

Here’s below the summary of the study cases for the hydraulic model:

``` r
dfRivCfg <- read.csv(system.file("river_configurations.tsv",
                                 package = "airGR.FlowPropagationTests"),
                     sep = "\t")
knitr::kable(dfRivCfg, row.names = TRUE)
```

|     |    S1 |  L1 |  B1 |  K1 |    S2 |  L2 |  B2 |  K2 |    S3 |  L3 |  B3 |  K3 |
|:----|------:|----:|----:|----:|------:|----:|----:|----:|------:|----:|----:|----:|
| 1   | 1e-03 |   1 |  20 |  33 | 0.001 |   1 |  20 |  33 | 0.001 | 100 |  20 |  33 |
| 2   | 1e-03 | 100 |  20 |  33 | 0.001 |   1 |  20 |  33 | 0.001 |  50 |  20 |  33 |
| 3   | 1e-03 |  50 |  20 |  33 | 0.001 |  50 |  20 |  33 | 0.001 |  50 |  20 |  33 |
| 4   | 5e-04 |  50 |  10 |  16 | 0.005 |  50 |  20 |  33 | 0.001 |  50 |  20 |  33 |
| 5   | 5e-04 |  50 |  20 |  16 | 0.005 |  50 |  10 |  33 | 0.001 |   1 |  20 |  33 |

## Maximum flow capacity

``` r
depth_max <- max(dfRivCfg[, grep("B", names(dfRivCfg))]) / 10
K_min <- min(dfRivCfg[, "K1"])
S_min <- min(dfRivCfg[, "S1"])
B_min <- min(dfRivCfg[, "B1"])
Q_max <- calc_uniform_Q(y = depth_max,
                        Ks = K_min,
                        slope = S_min,
                        s_down_width = B_min,
                        s_bank_slope = 0)
```

Maximal discharge used for the flood will be 9.0761769 m<sup>3</sup>/s.

## Flood configuration

The same flood time series is injected in both upstream nodes. It’s a
Gaussian curve with the following characteristics:

``` r
inflows <- get_upstream_flow(time_step = 3600,
                             time_end = 5 * 24 * 60 * 60,
                             base_flow = Q_max / 10,
                             peak_flow = Q_max,
                             peak_time = 36 * 3600,
                             peak_width = 6 * 3600)

ggplot(as.data.frame(inflows), aes(x = t, y = Q)) + geom_line()
```

<img src="man/figures/README-unnamed-chunk-3-1.png" width="100%" />

# Simulations and results

``` r
run_and_display <- function(studyCase, dfRivCfg, inflows, time_step = 3600) {
  file <- sprintf("study_case_%02d.tsv", studyCase)
  if (file.exists(file)) {
    read.table(file, sep = "\t")
  }
  rivCfg = dfRivCfg[studyCase, ]
  cfg <- generate_sic_model(rivCfg, s_depth = depth_max)
  outflow <- run_simulation(cfg, rivCfg, upstream_flow = inflows, time_step = time_step)
  df <- rbind(
    data.frame(t = inflows[, 1], Q = inflows[, 2], location = "upstream"),
    data.frame(t = outflow[, 1], Q = outflow[, 2], location = "downstream")
  )
  write.table(df, file, sep = "\t")
  return(df)
}
l <- lapply(seq(nrow(dfRivCfg)), run_and_display, dfRivCfg = dfRivCfg, inflows = inflows)
#> `sic_path` defined by environment variable SICPATH=C:\DocDD\Inrae\MyDrive\SIC
#> `sic_path` defined by environment variable SICPATH=C:\DocDD\Inrae\MyDrive\SIC
#> `sic_path` defined by environment variable SICPATH=C:\DocDD\Inrae\MyDrive\SIC
#> `sic_path` defined by environment variable SICPATH=C:\DocDD\Inrae\MyDrive\SIC
#> `sic_path` defined by environment variable SICPATH=C:\DocDD\Inrae\MyDrive\SIC
```

``` r
invisible(
  lapply(seq(nrow(dfRivCfg)), function(studyCase) {
    cat("\n\n### Study case #", studyCase, "\n\n")
    sCfg <- paste(paste0(names(dfRivCfg), "=", dfRivCfg[studyCase, ]), collapse = " ")
    p <- ggplot(l[[studyCase]], aes(x = t, y = Q)) +
      geom_line(aes(color = location)) +
      scale_x_time() + ggtitle(paste0("Study case #", studyCase), sCfg)
    print(p)
  })
)
```

### Study case \# 1

<img src="man/figures/README-unnamed-chunk-5-1.png" width="100%" />

### Study case \# 2

<img src="man/figures/README-unnamed-chunk-5-2.png" width="100%" />

### Study case \# 3

<img src="man/figures/README-unnamed-chunk-5-3.png" width="100%" />

### Study case \# 4

<img src="man/figures/README-unnamed-chunk-5-4.png" width="100%" />

### Study case \# 5

<img src="man/figures/README-unnamed-chunk-5-5.png" width="100%" />
