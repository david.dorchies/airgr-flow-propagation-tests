test_that("multiplication works", {
  dfRivCfg <- read.csv(
    system.file("river_configurations.tsv", package = "airGR.FlowPropagationTests"),
    sep = "\t"
  )
  l_cfg <- apply(dfRivCfg[1:2,], 1, generate_sic_model, s_depth = 2)
  expect_type(l_cfg, "list")
})
